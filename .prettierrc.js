module.exports = {
  singleQuote: true,
  jsxSingleQuote: true,
  trailingComma: 'all',
  useTabs: true,
  endOfLine: 'auto',
};
