import {
	DrawerContentComponentProps,
	DrawerContentOptions,
} from '@react-navigation/drawer';
import { Drawer, DrawerItem, IndexPath } from '@ui-kitten/components';
import React from 'react';

export type RootStackParamList = {
	Reference: undefined;
	StudentPortal: undefined;
};

export const DrawerContent = (
	props: DrawerContentComponentProps<DrawerContentOptions>,
) => {
	return (
		<Drawer
			selectedIndex={new IndexPath(props.state.index)}
			onSelect={(index) =>
				props.navigation.navigate(props.state.routeNames[index.row])
			}
		>
			<DrawerItem title='Reference' />
			<DrawerItem title='Student Portal' />
		</Drawer>
	);
};
