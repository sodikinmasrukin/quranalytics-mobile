import { Text } from '@ui-kitten/components';
import React from 'react';
import { View } from 'react-native';
import { Ayah } from '../../../data/Ayah';

type Props = {
	ayah: Ayah;
};

export const AyahDisplay = (props: Props) => {
	return (
		<View style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
			<Text style={{ marginTop: 6, marginBottom: 6 }}>
				{props.ayah.surahNumber}:{props.ayah.ayahNumber}
			</Text>
			<View
				style={{
					width: '100%',
					flexDirection: 'row',
					justifyContent: 'flex-end',
					marginTop: 6,
					marginBottom: 6,
				}}
			>
				<Text category='h4'>
					{props.ayah.arabicWords.map((word) => word.arabicText).join(' ')}
				</Text>
			</View>
			<Text category='p1' style={{ marginTop: 6, marginBottom: 6 }}>
				{props.ayah.translation}
			</Text>
			{/* <Text category='p2' style={{ marginTop: 6, marginBottom: 6 }}>
				{props.ayah.transliteration}
			</Text> */}
		</View>
	);
};
