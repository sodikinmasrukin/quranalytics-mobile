import { DrawerNavigationProp } from '@react-navigation/drawer';
import {
	Icon,
	Layout,
	TopNavigation,
	TopNavigationAction,
} from '@ui-kitten/components';
import React from 'react';
import { FlatList, View } from 'react-native';
import { Ayah } from '../../../data/Ayah';
import { RootStackParamList } from '../../../Navigation';
import { makeApiRequest } from '../../../util';
import { AyahDisplay } from './AyahDisplay';

type Props = {
	navigation: DrawerNavigationProp<RootStackParamList, 'Reference'>;
};

export const Reference = (props: Props) => {
	let [ayahs, setAyahs] = React.useState<Ayah[]>();

	React.useEffect(() => {
		(async () => {
			const result = await makeApiRequest({
				endpoint: 'search',
				type: 'phrase',
				searchColumn: 'arabic',
				translation: 'sahih_international',
				surahNo: 1,
			});
			setAyahs(result.data.map((ayahData: any) => Ayah.fromJson(ayahData)));
		})();
	}, []);

	return (
		<>
			<TopNavigation
				title='Reference'
				accessoryLeft={() => (
					<TopNavigationAction
						icon={(iconProps) => <Icon {...iconProps} name='menu-outline' />}
						onPress={() => {
							props.navigation.toggleDrawer();
						}}
					/>
				)}
			/>
			<Layout
				style={{
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-start',
					padding: 16,
				}}
			>
				{ayahs !== undefined && (
					<FlatList
						data={ayahs}
						renderItem={({ item }) => (
							<View style={{ paddingBottom: 20 }}>
								<AyahDisplay ayah={item} />
							</View>
						)}
						keyExtractor={(ayah) => ayah.ayahNumber.toString()}
					/>
				)}
			</Layout>
		</>
	);
};
