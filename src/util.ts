export const objToQueryString = (obj: { [key: string]: any }) => {
	const keyValuePairs = [];
	for (const key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			const value = obj[key];
			keyValuePairs.push(
				encodeURIComponent(key) + '=' + encodeURIComponent(value),
			);
		}
	}
	return keyValuePairs.join('&');
};

export const makeApiRequest = async (params: { [key: string]: any }) => {
	let response = await fetch(
		'http://quranalytics.org/api.php?' + objToQueryString(params),
	);
	return response.json();
};
