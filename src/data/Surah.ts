import { Ayah } from './ayah';

export interface SurahFields {
	surahNumber: number;
	title: string;
	titleTranslation: string;
	ayahs: Ayah[];
}

export class Surah {
	public surahNumber: number;
	public title: string;
	public titleTranslation: string;
	public ayahs: Ayah[];

	public constructor(fields: SurahFields) {
		this.surahNumber = fields.surahNumber;
		this.title = fields.title;
		this.titleTranslation = fields.titleTranslation;
		this.ayahs = fields.ayahs;
	}
}
